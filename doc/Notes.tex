\documentclass[12pt]{report}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{textcomp}
\usepackage{lmodern}

\usepackage[english]{babel}

\usepackage[dvipsnames]{xcolor}
\usepackage[colorlinks=true]{hyperref}

\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}

\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{bm}

\graphicspath{{img/}}

% Collection of useful mathematical symbols and commands
% Calculus
\newcommand{\ud}{\mathrm{d}}
\newcommand{\pder}[2]{\frac{\partial{#1}}{\partial{#2}}}
\newcommand{\dpder}[2]{\frac{\partial^2{#1}}{\partial{#2^2}}}
\newcommand{\sderp}[3]{\frac{\partial^2{#1}}{\partial{#2}\partial{#3}}}
\newcommand{\tder}[2]{\frac{\ud{#1}}{\ud{#2}}}
\newcommand{\rot}[1]{\nabla \times {#1}}
\newcommand{\diver}[1]{\nabla \cdot {#1}}
\newcommand{\definter}[4]{\int_{#1}^{#2} {#3}\ud {#4}}
\newcommand{\inter}[2]{\int {#1}\ud {#2}}
\newcommand{\braket}[2]{\langle {#1} , {#2} \rangle}
% Misc
\newcommand{\eval}[1]{\Big |_{#1}}
\newcommand{\bset}[1]{\big\lbrace {#1} \big\rbrace}
\newcommand{\stimes}[2]{{{#1}\!\times\!{#2}}}
\newcommand{\trp}{\top}
\newcommand{\preup}[2]{{}^{#2}\!{#1}}

% Operators
\DeclareMathOperator*{\armin}{arg\,min}
\DeclareMathOperator*{\armax}{arg\,max}
\DeclareMathOperator*{\rank}{rank}
\DeclareMathOperator*{\cov}{cov}
\DeclareMathOperator*{\nullsp}{null}
% Logicals
\newcommand{\suchthat}{\big \backslash \;}

\newcommand{\JPC}[1]{\textcolor{olive}{JPC: {#1}}}

\begin{document}

\title{Time derivatives of coordinates of smooth autonomous dynamical systems}
\author{Juan Pablo Carbajal}
\maketitle

\section{Preliminaries}
Autonomous dynamical system 

\begin{align}
\tder{\bm{q}}{t} &= \bm{F}(\bm{q})\\
\bm{q} &= \begin{bmatrix}q_1\\\vdots\\q_n\end{bmatrix} \in \mathbb{R}^n, \quad
\bm{F}(\bm{q}) = \begin{bmatrix}f_1(\bm{q})\\\vdots\\f_n(\bm{q})\end{bmatrix} \in \mathcal{F}\left(\mathbb{R}^n\right)
\end{align}

\noindent where each $f_i(\bm{q}): \mathbb{R}^n \rightarrow \mathbb{R}$ and with the needed smootheness.

We are interested in the the time trace of a single component of $\bm{q}$, without lossing generality we take $x(t) = q_1(t)$.
For clarity of notation we will omit the subindex, i.e. $f_1 = f$.

Consider a multivariate scalar function $h(\bm{q})$.
We then have

\begin{align}
\nabla h = \begin{bmatrix}\pder{h}{q_1} \\ \vdots \\ \pder{h}{q_n} \end{bmatrix} \quad & \text{(Gradient)}\\
H_h = \begin{bmatrix}\pder{\nabla h^\trp}{q_1} \\ \vdots \\ \pder{\nabla h^\trp}{q_n} \end{bmatrix} \quad & \text{(Hessian)}
\end{align}

We will aso use the Jacobian of the vector field $\bm{F}$

\begin{equation}
J_{\bm{F}} = \begin{bmatrix}\nabla f_1^\trp \\ \vdots \\ \nabla f_n^\trp \end{bmatrix}
\end{equation}
\noindent and its derivative with respect to time

\begin{gather}
\begin{aligned}
  \tder{\bm{F}}{t} = &\begin{bmatrix}\tder{f_1}{t} \\ \vdots \\ \tder{f_n}{t} \end{bmatrix} = \begin{bmatrix}\nabla f_1^\trp \bm{F} \\ \vdots \\ \nabla f_n^\trp \bm{F} \end{bmatrix} = \\
  = & J_{\bm{F}} \bm{F}
\end{aligned}
\end{gather}

\section{Time derivatives}

\begin{equation}
\tder{x}{t} = f
\end{equation}

\begin{equation}
\tder{{}^2x}{t^2} = \tder{f}{t} = \nabla f^\trp \bm{F}
\end{equation}

\begin{gather}
\begin{aligned}
  \tder{{}^3x}{t^3} = & \tder{\nabla f^\trp \bm{F}}{t} = \tder{\nabla f}{t}^\trp \bm{F} + \nabla f^\trp \tder{\bm{F}}{t} = \\
  = & \bm{F}^\trp H_{f} \bm{F} + \nabla f^\trp  J_{\bm{F}} \bm{F}
\end{aligned}
\end{gather}

\noindent where we used

\begin{align}
\left(\tder{\nabla f}{t}\right)_i &= \tder{}{t}\pder{f}{q_i} = \sum_{j=1}^n \pder{{}^2f}{q_jq_i}\tder{q_j}{t} = \sum_{j=1}^n \left(H_f\right)_{ji} f_j = \\
\tder{\nabla f}{t} &= H_{f}^\trp \bm{F}
\end{align}

\section{Examples}

\subsection{1D example}
\begin{equation}
f = \bm{F} = -x  (x - 1) (x - 2) (x - 3)
\end{equation}

\begin{equation}
\nabla f = J_{\bm{F}} = (x - 2)(x - 3)(1 - 2x) - x(x - 1)(2x - 5)
\end{equation}

\begin{figure}[htpb]
\centering
\includegraphics[width=0.9\textwidth]{demo1d_timeseries.png}
\caption{Time series 1D system}
\label{fig:1d}
\end{figure}

\subsection{Van der Pol}

\begin{equation}
\bm{F} = \begin{bmatrix} y \\ y (1 - x)^2 - x\end{bmatrix}
\end{equation}

\begin{equation}
J_{\bm{F}} = \begin{bmatrix} 0 & 1 \\ -2xy - 1 & (1 - x)^2\end{bmatrix}
\end{equation}

\begin{figure}[hbpt]
  \centering
  \begin{subfigure}[c]{0.5\textheight}
    \includegraphics[width=\textwidth]{vanderpol_phasespace.png}
    \caption{\label{fig:vdp_ps}}
  \end{subfigure}\\
  \begin{subfigure}[c]{0.5\textheight}
    \includegraphics[width=\textwidth]{vanderpol_timeseries.png}
    \caption{\label{fig:vdp_ts}}
  \end{subfigure}
  \caption{
    \subref{fig:vdp_ps}) Phase space.
    \subref{fig:vdp_ts}) Time series
  }
  \label{fig:vdp}
\end{figure}

%\bibliographystyle{unsrtnat}
%\bibliography{references}

\end{document}
