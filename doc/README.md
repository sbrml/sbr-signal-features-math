## Documentation folder

Contains documents, mainly latex.

Make sure you generated the figures running the scripts 
in the `mfiles` folder.
