## Demostration of time derivatives formula in 2D
#
##

# Copyright (C) 2018 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Created: 2018-10-19

clear all
close all

% vector field of derivatives
% F: dim x nT --> dim x nT
function dqdt = F(x)
  dqdt = zeros (size (x));
  dqdt(1,:) = x(2,:); 
  dqdt(2,:) = (1 - x(1,:).^2) .* x(2,:) - x(1,:);
endfunction

% Jacobian of vector field
% JF: dim x nT --> dim x dim x nT
function JacF = JF(x)
  [dim nT] = size (x);
  JacF = zeros (dim, dim, nT);
  JacF(1,:,:) = [zeros(1, nT); ones(1, nT)];
  JacF(2,:,:) = [-2*x(1,:).*x(2,:); (1 - x(1,:).^2)] - [ones(1, nT); zeros(1, nT)];
endfunction

% Auxiluary functions
delf =@(q) squeeze (JF(q)(1,:,:)); % gradient of first derivative

## Time integration
nT  = 200;
t   = linspace (0, 13.5, nT).';
q0  = {[2, 0], [0.1, 0], [4, 0], [-2, 2]};
nS  = numel (q0);
dim = size (q0{1}, 2);
Q   = zeros (nT, dim, nS);
x   = dotx = ddotx = zeros (nT, nS);
opt = odeset ('Jacobian', @(t,q)JF(q));
for i=1:nS
  [~,q] = ode45 (@(t,q)F(q), t, q0{i}, opt);
  Q(:,:,i) = q;
  q = q.';
  x(:,i)     = q(1,:);
  dotx(:,i)  = F(q)(1,:);
  ddotx(:,i) = sum (delf(q) .* F(q), 1);
endfor

figure (1), clf
   plot (squeeze (Q(:,1,:)), squeeze (Q(:,2,:)));
   % TODO add quiver vector field
   axis tight
   xlabel ('x')
   ylabel ('y')
   set (gca, 'box', 'off', 'xaxislocation', 'origin', 'yaxislocation', 'origin');

figure (2), clf
  subplot(3, 1, 1)
  plot (t, x, 'linewidth', 2);
  axis tight
  line (xlim, 0)
  ylabel ('x(t)')

  subplot(3, 1, 2)
  plot (t, dotx, 'linewidth', 2);
  axis tight
  line (xlim, 0)
  ylabel ('dxdt(t)')

  subplot(3, 1, 3)
  plot (t, ddotx, 'linewidth', 2);
  axis tight
  line (xlim, 0)
  xlabel ('Time')
  ylabel ('d2xdt2(t)')
  
figure(1)
print -dpng "../doc/img/vanderpol_phasespace.png"
figure(2)
print -dpng "../doc/img/vanderpol_timeseries.png"
