## Demostration of time derivatives formula in 1D
#
##

# Copyright (C) 2018 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Created: 2018-10-19

clear all
close all

% vector field of derivatives
% F: dim x nT --> dim x nT
F =@(x) -x .* (x - 1) .* (x - 2) .* (x - 3);

% Jacobian of vector field
% JF: dim x nT --> dim x dim x nT
JF =@(x) (1 - 2 * x) .* (x - 2) .* (x - 3) - x .* (x - 1) .* (2 * x - 5);
% Auxiluary functions
f =@(x) F(x)(1,:);     % first derivative w.r.t. x(t)
delf =@(x) JF(x)(1,:); % gradient of first derivative

% critical poits of dx/dt (zeros of f(x))
cp           = [0, 1, 2, 3];
% 2nd derivtive w.r.t. time
dfdt =@(x) delf(x) .* F(x); % 1d case of delf.' * F
cp_stability = sign (f (cp+0.01));

## Time integration
nT = 100;
t  = linspace (0, 5, nT).';
x0 = {1.99, 0.01, 2.01};
nS = numel (x0);
x  = dotx = ddotx = zeros (nT, nS);
opt = odeset ('Jacobian', @(t,x)JF(x));
for i=1:nS
  [~,x(:,i)] = ode45 (@(t,x)F(x), t, x0{i}, opt);
  dotx(:,i) = f(x(:,i).');
  ddotx(:,i) = dfdt(x(:,i).');
endfor

figure (1), clf
  xlims = [0, t(end)];
  ylims = [-0.1, 3.1];
  subplot(3, 1, 1)
  plot (t, x, 'linewidth', 2);
  for i=1:length(cp)
    hl = line (xlim(), cp(:,i));
    if cp_stability(i) > 0
      set (hl, 'linestyle', '--');
    endif
  endfor
  xlim (xlims)
  ylim (ylims)
  ylabel ('x(t)')

  subplot(3, 1, 2)
  plot (t, dotx, 'linewidth', 2);
  line (xlims, 0)
  axis tight
  ylabel ('dxdt(t)')

  subplot(3, 1, 3)
  plot (t, ddotx, 'linewidth', 2);
  line (xlims, 0)
  axis tight
  xlabel ('Time')
  ylabel ('d2xdt2(t)')
  
print -dpng "../doc/img/demo1d_timeseries.png"
